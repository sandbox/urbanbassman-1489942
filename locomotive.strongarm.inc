<?php
/**
 * @file
 * locomotive.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function locomotive_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_locomotive';
  $strongarm->value = 0;
  $export['comment_anonymous_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_locomotive';
  $strongarm->value = 1;
  $export['comment_default_mode_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_locomotive';
  $strongarm->value = '50';
  $export['comment_default_per_page_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_locomotive';
  $strongarm->value = 1;
  $export['comment_form_location_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_locomotive';
  $strongarm->value = '2';
  $export['comment_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_locomotive';
  $strongarm->value = '1';
  $export['comment_preview_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_locomotive';
  $strongarm->value = 1;
  $export['comment_subject_field_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_locomotive';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_locomotive';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_locomotive';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_locomotive';
  $strongarm->value = '1';
  $export['node_preview_locomotive'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_locomotive';
  $strongarm->value = 1;
  $export['node_submitted_locomotive'] = $strongarm;

  return $export;
}
