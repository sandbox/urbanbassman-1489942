<?php
/**
 * @file
 * locomotive.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function locomotive_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function locomotive_node_info() {
  $items = array(
    'locomotive' => array(
      'name' => t('Locomotive'),
      'base' => 'node_content',
      'description' => t('An <em>locomotive</em> allows you to add details of locomotives'),
      'has_title' => '1',
      'title_label' => t('Identification'),
      'help' => t('<p>This form allows you to enter details of a locomotive.</p>
<p>In the Identification field enter a description of the locomotive in the format [company] [class] [number] [name] (where applicable)</p>
<p>For example \'LMS Princess Coronation 6229 Duchess of Hamilton\' or \'BR Class 31 31018\''),
    ),
  );
  return $items;
}
